from django.contrib import admin

from .models import Play, User
# Register your models here.

@admin.register(Play)
class PlayAdmin(admin.ModelAdmin):
    list_display = ('name', 'date')


@admin.register(User)
class PlayAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'gender', 'phone_number', 'email', 'password')
