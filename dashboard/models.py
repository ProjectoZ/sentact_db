from django.db import models

# Create your models here.

class Play(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateTimeField()


class User(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    gender = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length =100)

    def __str__(self):
        # Built-in attribute of django.contrib.auth.models.User !
        return self.user.first_name
