from django.conf.urls import url
from dashboard import views

urlpatterns = [
    url(r'^.*\.html', views.sentact_html, name='sentact'),

    # The home page
    url(r'^$', views.index, name='index'),
]