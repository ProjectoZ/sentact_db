from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from random import randint
from django.views.generic import TemplateView
from django.contrib.auth import get_user_model

from django.http import JsonResponse
from django.views.generic import View

from rest_framework.views import APIView
from rest_framework.response import Response

from django.db import connections
from django.db.models import Count

from .models import Play

# Create your views here.
def index(request):
    context = {}
    template = loader.get_template('dashboard/index.html')
    return HttpResponse(template.render(context, request))


def sentact_html(request):
    context = {}
    load_template = request.path.split('/')[-1]
    template = loader.get_template('dashboard/' + load_template)
    return HttpResponse(template.render(context, request))


def graph(request):
    return render(request, 'dashboard/graph/graph.html')


def play_count_by_month(request):
    data = Play.objects.all() \
        .extra(
            select={
                'month': connections[Play.objects.db].ops.date_trunc_sql('month', 'date')
            }
        ) \
        .values('month') \
        .annotate(count_items=Count('id'))
    return JsonResponse(list(data), safe=False)
